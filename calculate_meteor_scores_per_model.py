from argparse import ArgumentParser
from keras.preprocessing import image
from keras.models import load_model
import numpy as np
import os
import json

from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import corpus_bleu
from nlgeval import NLGEval
from nlgeval import compute_metrics

from caption_utils import *
from image_captioning import *
from tqdm import tqdm
from gpu_config import set_config


def get_captions(model, img_path, input_shape, encoder_model, decoder_model):
    beam_size = 5
    max_length = 15
    len_norm = True
    alpha = 0.4
    return_probs = True
    img = image.load_img(img_path, target_size=(299, 299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    features = model.predict(x)
    return beam_search(
        features,
        encoder_model=encoder_model,
        decoder_model=decoder_model,
        input_shape=input_shape,
        beam_size=beam_size,
        max_length=max_length,
        len_norm=len_norm,
        alpha=alpha,
        return_probs=return_probs)


def get_reference_and_candidates(model, fns_list, input_shape, encoder_model, decoder_model):
    all_refs = []
    all_candidates = []
    references = []

    for i, filename in enumerate(tqdm(fns_list, unit='files')):
        candidate = get_captions(model, "data/Flicker8k_Dataset/" +
                                 filename, input_shape, encoder_model, decoder_model)
        for j, caption in enumerate(test_captions_raw[filename]):
            references.append(caption[:-1])
        # all_refs.append(references)
        for item in candidate:
            all_candidates.append(item)
    return references, all_candidates


def write_refs_file(data):
    filepath = "refs.txt"
    with open(filepath, 'w') as file_handler:
        for item in data:
            file_handler.write("{}\n".format(item))


def write_cadidate_file(data):
    filepath = "candidate.txt"
    with open(filepath, 'w') as file_handler:
        for item in data:
            file_handler.write("{}\n".format(item))


if __name__ == "__main__":
    set_config()

    trained_model = "InceptionV3_avg_try.h5"
    encoder_model = "encoder_model_InceptionV3_avg_try.h5"
    decoder_model = "decoder_model_InceptionV3_avg_try.h5"

    train_captions_raw, dev_captions_raw, test_captions_raw = get_caption_split()
    vocab = create_vocab(train_captions_raw)
    global token2idx, idx2token
    token2idx, idx2token = vocab_to_index(vocab)

    train_filename_list, dev_filename_list, test_filename_list = load_split_lists()

    from keras.applications.inception_v3 import InceptionV3
    from keras.applications.inception_v3 import preprocess_input
    pretrained_model = InceptionV3(
        weights='imagenet', include_top=False, pooling='avg')
    input_shape = 2048

    print("\nLoading models...")
    encoder_model = load_model(
        "saved_models/encoder_model_InceptionV3_avg_try.h5")
    decoder_model = load_model(
        "saved_models/decoder_model_InceptionV3_avg_try.h5")

    print("\nCalculating NLG scores...")
    if os.path.exists('candidate.txt') and os.path.exists('refs.txt'):
        pass
    else:
        all_refs, all_candidates = get_reference_and_candidates(
            pretrained_model, test_filename_list, input_shape, encoder_model, decoder_model)
        print("\nGenerating reference text file...")
        write_refs_file(all_refs)
        print("\nGenerating candidate text file...")
        write_cadidate_file(all_candidates)

    scores = compute_metrics(hypothesis='candidate.txt',
                             references=['refs.txt'])
