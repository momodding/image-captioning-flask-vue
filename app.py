import os
import requests
from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
from image_captioning import get_captions
from random import *
from flask_cors import CORS

app = Flask(__name__, static_folder="./dist/static", template_folder="./dist")

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

# UPLOAD_FOLDER = os.path.join('frontend', 'src', 'assets', 'uploads')
UPLOAD_FOLDER = os.path.join('flask', 'static', 'uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    if app.debug:
        return requests.get('http://localhost:8080/{}'.format(path)).text
    return render_template("index.html")


@app.route('/api/upload', methods=['POST'])
def upload_file():
    file = request.files['file']
    f = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
    file.save(f)
    caption = get_captions(f)
    # response = {'caption': 'test_caption', 'image': '..\..\..'+f}
    response = {'caption': caption, 'image': file.filename}
    return jsonify(response)


if __name__ == "__main__":
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=True, host='0.0.0.0', threaded=True)
