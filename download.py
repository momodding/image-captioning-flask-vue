from urllib import request
from tqdm import *

if __name__ == '__main__':
    # print(get_filenames('source.txt'))
    with open('source.txt') as f:
        i = 0
        for line in tqdm(f):
            i = i+1
            try:
                pic = open(str(i)+'.jpg', 'wb')
                pic.write(request.urlopen(line).read())
                pic.close()
            except:
                pass
