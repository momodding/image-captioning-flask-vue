from argparse import ArgumentParser
from keras.preprocessing import image
from keras.models import load_model
import numpy as np

from nltk.translate.bleu_score import sentence_bleu
from nltk.translate.bleu_score import corpus_bleu

from caption_utils import *
from image_captioning import *
from tqdm import tqdm
from gpu_config import set_config


def generate_seq(img_input, input_shape, encoder_model, decoder_model):

    if img_input.shape != (1, input_shape):
        img_input = img_input.reshape(1, input_shape)

    assert(img_input.shape == (1, input_shape))
    stop_condition = False
    decoded_sentence = []
    target_seq = np.array([token2idx['<bos>']]).reshape(1, 1)
    states_value = encoder_model.predict(img_input)

    while not stop_condition:
        output_tokens, h, c = decoder_model.predict(
            [target_seq] + states_value)
        sampled_token_index = int(np.argmax(output_tokens[0, -1, :]))
        sampled_char = idx2token[sampled_token_index]
        decoded_sentence += [sampled_char]
        if (sampled_char == '<eos>' or len(decoded_sentence) > 30):
            stop_condition = True
        target_seq = np.array([sampled_token_index]).reshape(1, 1)
        states_value = [h, c]

    return ' '.join(decoded_sentence[:-1])


def get_captions(model, img_path, input_shape, encoder_model, decoder_model):
    beam_size = 20
    max_length = 20
    len_norm = True
    alpha = 0.7
    return_probs = False
    img = image.load_img(img_path, target_size=(299, 299))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)

    features = model.predict(x)
    return beam_search(
        features,
        encoder_model=encoder_model,
        decoder_model=decoder_model,
        input_shape=input_shape,
        beam_size=beam_size,
        max_length=max_length,
        len_norm=len_norm,
        alpha=alpha,
        return_probs=return_probs)


def get_reference_and_candidates(model, fns_list, input_shape, encoder_model, decoder_model):
    all_refs = []
    all_candidates = []

    for i, filename in enumerate(tqdm(fns_list, unit='files')):
        candidate = get_captions(model, "data/Flicker8k_Dataset/" +
                                 filename, input_shape, encoder_model, decoder_model).split()
        references = []
        for j, caption in enumerate(test_captions_raw[filename]):
            references.append(caption[:-1].split())
        all_refs.append(references)
        all_candidates.append(candidate)
    return all_refs, all_candidates


def calculate_corpus_bleu_scores(all_refs, all_candidates):
    corpus1 = corpus_bleu(all_refs, all_candidates, weights=(1, 0, 0, 0)) * 100
    corpus2 = corpus_bleu(all_refs, all_candidates, weights=(0, 1, 0, 0)) * 100
    corpus3 = corpus_bleu(all_refs, all_candidates, weights=(0, 0, 1, 0)) * 100
    corpus4 = corpus_bleu(all_refs, all_candidates, weights=(0, 0, 0, 1)) * 100
    return corpus1, corpus2, corpus3, corpus4


if __name__ == "__main__":
    set_config()

    trained_model = "InceptionV3_avg_try.h5"
    encoder_model = "encoder_model_InceptionV3_avg_try.h5"
    decoder_model = "decoder_model_InceptionV3_avg_try.h5"

    train_captions_raw, dev_captions_raw, test_captions_raw = get_caption_split()
    vocab = create_vocab(train_captions_raw)
    global token2idx, idx2token
    token2idx, idx2token = vocab_to_index(vocab)

    train_filename_list, dev_filename_list, test_filename_list = load_split_lists()

    from keras.applications.inception_v3 import InceptionV3
    from keras.applications.inception_v3 import preprocess_input
    pretrained_model = InceptionV3(
        weights='imagenet', include_top=False, pooling='avg')
    input_shape = 2048

    print("\nLoading models...")
    encoder_model = load_model(
        "saved_models/encoder_model_InceptionV3_avg_try.h5")
    decoder_model = load_model(
        "saved_models/decoder_model_InceptionV3_avg_try.h5")

    print("\nCalculating bleu scores...")
    all_refs, all_candidates = get_reference_and_candidates(
        pretrained_model, test_filename_list, input_shape, encoder_model, decoder_model)
    print("\nCorpus BLEU score...")
    corpus_bleu_scores = calculate_corpus_bleu_scores(all_refs, all_candidates)

    for i, corpus_score in enumerate(corpus_bleu_scores):
        print("Bleu{}: {:0.2f}".format(i+1, corpus_score))
