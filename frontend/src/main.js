import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueProgressBar from 'vue-progressbar'
import Icon from 'vue-awesome/components/Icon'
import App from './App'
import router from './router'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-awesome/icons'

const options = {
  color: '#bffaf3',
  failedColor: '#874b4b',
  thickness: '5px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
  autoRevert: true,
  location: 'left',
  inverse: false
}
Vue.config.productionTip = false
Vue.component('icon', Icon)
Vue.use(BootstrapVue, VueProgressBar, options)

/* eslint-disable no-new */
// new Vue({
//   router,
//   render: h => h(App)
// }).$mount('#app')
new Vue({
  ...App,
  router
}).$mount('#app')

// router.beforeEach((to, from, next) => {
//   router.app.$Progress.start()
//   next()
// })

// router.afterEach(() => {
//   router.app.$Progress.finish()
// })
